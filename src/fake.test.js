const sinon = require("sinon")

describe ('Some more test using fake',() => {
	
  it("should be able to used  fake instead of spies", () => {
        const foo = {
            bar: () => "baz",
        };
        // wrap existing method without changing its behaviour
        const fake = sinon.replace(foo, "bar", sinon.fake(foo.bar));

        expect(fake()).toBe("baz"); // behaviour is the same
        expect(fake.callCount).toBe(1); // calling information is saved
    })

   it("should be able to used fake instead of stubs", () => {
        const foo = {bar: () => "baz",}
        // replace method with a fake one
        const fake = sinon.replace(foo,"bar",sinon.fake.returns("fake value"))

        expect(fake()).toBe("fake value") // returns fake value
        expect(fake.callCount).toBe(1) // saves calling information
    })
}) 